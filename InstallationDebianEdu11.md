## Installation DebianEdu11 
Zoubeïda Abdelmoula (26/08/2021)

Connecter la machine dedié au nouveau tjener à un réseau avec DHCP (il lui permet de passer l'étape config réseau)
brancher la clé bootable avec l'iso DebianEdu11 et la clé contenant les firmwares

metter la machien sous tension et la démarrer (mode boot F11 sur notre machine)

- choisir la langue et le clavier
- config réseau par défaut
- profils choisi : serveur principal  
- partitionnement automatique choisi
- participation au stat ok
- mot de passe root à ajouter
- utilisateur principal créé (pas de nom ou prénom composé ou de caratères spéciaux) + mot de passe (sudoer dans ce cas précis)
- choisir le bon disque à partitionner (pas les clé USB !!) 
- forcer l'installation UFI OK 

Séquence d'installation des paquets !!!

Une fois terminée, on enlève les clés usb et on redémarre le système !

On tente une MAJ : `sudo apt update`
- fichier `/etc/apt/sources.list`modifié avec les liens deb correspondants
- connection impossible au liens miroirs de MAJ -> changement du fichier `/etc/network/interfaces` pour désigner la bonne interface réseau selectionnée lors de l'installation (`ip l` et `ip r` et `ping ip_routeur` on été utilisée pour le diagnostique)

- Problème de résolution de nom extérieur -> changement du fichier `/etc/bind/named.conf.options`  et modifier l'ip des forwarders avec les prochains DNS de confiance : 8.8.8.8 (google) ou celui du lycée (172.20.100.2), finalement FDN (80.67.169.12 et 80.67.169.40).

- Pour finir de résoudre le problème de la chaine de confiance, on a supprimé à la main (beurk) les fichier cache (`/var/cache/bind`). *** Trouver la bonne commande SVP***

`apt update` fonctionnel ! 

Accès à internet via navigateur avec une STA avec config mandataire automatique non fonctionnels :  
- Modification du fichier SQUID pour permettre les accès vers le mandataire du lycée (`/etc/squid/conf.d/debian-edu.conf`) ***conf à consigner dans git***

***A chaque étape, on a consigné chaque étape via etckeeper : `sudo etckeeper commit` cette commande sans l'option -m permet une visibilté des fichiers modifiés  

Lundi 30/08 : 

### création d'un 2e admin fred (à faire avec goza)

- Via GOZA, accéder à la liste des utilisateurs et créer un nouvel utilisateur (action "create user"). se caler sur l'ulisateur créé lors de l'installation (dans ce cas : za)
- Ajouter Fred aux groupes adms comme c'est déjà le cas pour za et aux groupes students et teachers
- ajouter le role fred dans sudoers

Test : en tentant une connexion avec l'utilisateur fred (ssh) et en tentant une action d'administration (`sudo apt update` par exemple)

### créer Alice BOB ayant tous les droits communs (teachers or students)

- Via GOZA, accéder à la liste des utilisateurs et créer un nouvel utilisateur (action "create user"). se caler sur l'ulisateur créé lors de l'installation (dans ce cas : za)
- Ajouter Fred aux groupes adms comme za et aux groupes students et teachers

Test : en tentant une connexion avec l'utilisateur alibo (ssh) et en tentant une action d'administration qui ne devrait pas fonctionner (`sudo apt update` par exemple)

### Install PXE 

- voir la doc et les manip pour PXE
- `/etc/debian-edu/pxeinstall.conf` fichier à modifier pour prise en compte lors du lancement
- `/etc/debian-edu/www/debian-edu-install.dat`à modifier pour répondre automatiquement aux différentes étapes de l'installation
- `/etc/debian-edu/www/debian-edu-install.dat`quelques exemples à suivre
- `/etc/debian-edu/www/debian-edu-install.dat` est remis à neuf à chaque démarrage de debian-edu-pxeinstall, Il faut donc ajouter le fichier  `/etc/debian-edu/www/debian-edu-install.dat.local` incluant vos conf additionnelles. 


### Encore à faire !!!
- et voir ce qu'il faut faire pour MAJ automatique (unattended)
